## create new export

Loop over all prefix from Prefixes_to_export.txt
- click CREATE NEW EXPORT button
- download the newly created xml file
- check if any xml files contain missing question/statement literal
- clean text within xml

*Note*: If all export button has been clicked, we can use [download_xml](https://gitlab.com/closer-cohorts1/archivist_export/download_xml) to get xml files without clicking buttons.
